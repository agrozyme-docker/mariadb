ARG DOCKER_REGISTRY=docker.io
ARG DOCKER_NAMESPACE=agrozyme
FROM ${DOCKER_REGISTRY}/${DOCKER_NAMESPACE}/alpine

RUN apk add --no-cache bash ruby-rdoc ruby-bundler tini-static su-exec patch git
SHELL [ "/bin/bash", "-c" ]

COPY rootfs /
RUN chmod +x /usr/local/bin/* \
  && gem install -N docker_core \
  && gem update -N docker_core \
  && gem clean \
  && /usr/local/bin/docker_build.rb
EXPOSE 3306

ENTRYPOINT ["/sbin/tini-static", "--"]

CMD ["/usr/local/bin/docker_run.rb"]
