# require_relative('../docker_core')
require('docker_core')

module DockerCore
  module Image
    module Mariadb
      module Build
        # @param [String] local_folder
        def self.update_my_cnf(local_folder)
          file = '/etc/my.cnf'
          text = File.read(file)
          text += "!includedir #{local_folder} \n"
          File.write(file, text)
          Shell.change_mode(644, file, "#{file}.d/*")
        end

        def self.main
          log_folder = '/var/log/mysql'
          local_folder = '/usr/local/etc/mysql'
          error_log = File.join(log_folder, 'error.log')

          System.run('apk add --no-cache mariadb mariadb-client mariadb-server-utils')
          Shell.make_folders(log_folder, local_folder)
          Shell.link_logs(stderr: error_log)
          # Shell.change_owner(error_log)
          System.invoke('Update my.cnf', self.method(:update_my_cnf), local_folder)
        end
      end

      module Run
        # @param [String] text
        def self.escape_keyword(text)
          data = "#{text}".gsub(/\\/, '\&\&').gsub(/'/, "''")
          return "'#{data}'"
          # return (text == data && 1 < data.length) ? data : "'#{data}'"
        end

        def self.build_profile
          profile = { 
            protocol: 'socket',
            user: 'root', 
            password: ENV.fetch('MYSQL_ROOT_PASSWORD', ''), 
            host: 'localhost', 
            database: '*', 
            debug: Paser.boolean(ENV['MYSQL_DEBUG']) 
          }

          return profile
        end
        
        # @param [String] user
        # @param [String] password
        # @param [String] host
        # @param [String] database
        def self.build_grant_statements(user, password, host, database)
          items = []

          if database.empty?
            return items
          end

          escaped_user = self.escape_keyword(user)
          escaped_password = self.escape_keyword(password)
          escaped_host = self.escape_keyword(host)
          # escaped_database = '*' == database ? '*' : self.escape_keyword(database)
          account = "#{escaped_user}@#{escaped_host}"

          if '*' != database
            items << "CREATE DATABASE IF NOT EXISTS #{database}"
          end

          if false == user.empty?
            items << "CREATE USER IF NOT EXISTS #{account} IDENTIFIED BY #{escaped_password}"
            items << "ALTER USER #{account} IDENTIFIED BY #{escaped_password}"
            items << "GRANT ALL ON #{database}.* TO #{account} WITH GRANT OPTION"
          end

          return items
        end

        def self.build_privileges_statements
          profile = self.build_profile
          user, password, host, database = profile.values_at( :user, :password, :host, :database)
          flush = 'FLUSH PRIVILEGES'
          
          items = [flush, "DELETE FROM mysql.user WHERE user IN ('') OR host NOT IN ('localhost', '%')"]
          items.concat(self.build_grant_statements(user, password, host, database))

          host = '%'
          items.concat(self.build_grant_statements(user, password, host, database))

          user = ENV.fetch('MYSQL_USER', '')
          password = ENV.fetch('MYSQL_PASSWORD', '')
          database = ENV.fetch('MYSQL_DATABASE', '')
          items.concat(self.build_grant_statements(user, password, host, database))

          items << flush
          return items
        end

        def self.check_database(protocol, user, password)
          count = 30
          ping = System.command('mysqladmin', { protocol: protocol, user: user, password: password }, 'ping')

          while 0 < count
            System.wait(1)

            if '' != System.capture(ping)
              break
            end

            Color.echo("- database initial System... #{count}", Color::YELLOW)
            count = count - 1
          end

          return 0 < count
        end

        def self.execute_statements(items, profile)
          if 0 < items.size
            protocol, user, password, debug = profile.values_at(:protocol, :user, :password, :debug)
            execute = items.join('; ') + ';'
            # System.run('mysql', { protocol: protocol, user: user, password: password,, 'init-command': 'SET @@SESSION.SQL_LOG_BIN=0;', execute: execute }, '', echo: debug)
            System.run('mysql', { protocol: protocol, user: user, password: password, execute: execute }, '', echo: debug)
          end
        end

        def self.shutdown_database(protocol, user, password)
          System.run('mysqladmin', { protocol: protocol, user: user, password: password }, 'shutdown', echo: false)
        end

        def self.setup_database
          System.run('mysqld_safe', { 'no-watch': true, 'skip-grant-tables': true, user: USER }, '')
          profile = self.build_profile
          protocol, user, password, debug = profile.values_at(:protocol, :user, :password, :debug)

          if false == self.check_database(protocol, user, '')
            raise('database initial process failed.')
          end

          System.invoke('Update Privileges Statements', self.method(:execute_statements), self.build_privileges_statements, profile)
          System.run('mysql_upgrade', { force: true, verbose: debug, protocol:protocol, user: user, password: password }, '')
          self.shutdown_database(protocol, user, password)
        end

        def self.install_database
          datadir = '/var/lib/mysql'
          Shell.make_folders(datadir)
          Shell.change_owner(datadir)

          if File.directory?("#{datadir}/mysql")
            return true
          end

          System.run('mysql_install_db', { user: USER, datadir: datadir }, '')
          return true
        end

        def self.setup_replication
          host = ENV.fetch('MYSQL_MASTER_HOST', '')
          port = ENV.fetch('MYSQL_MASTER_PORT', '3306')
          user = ENV.fetch('MYSQL_MASTER_USER', 'root')
          password = ENV.fetch('MYSQL_MASTER_PASSWORD', '')

          if host.empty?
            return
          end

          items = []
          items << 'RESET SLAVE'
          items << "CHANGE MASTER TO MASTER_HOST = '#{host}', MASTER_PORT = #{port}, MASTER_USER = '#{user}', MASTER_PASSWORD = '#{password}' "
          items << 'START SLAVE'

          self.execute_statements(items, self.build_profile)
        end

        def self.main
          Shell.update_user
          
          paths = %w[/run/mysqld /var/tmp]
          Shell.clear_folders(*paths)
          Shell.change_owner(*paths)
          
          # log_folder = '/var/log/mysql'
          # Shell.change_owner(log_folder)
          # Shell.change_mode(0777, log_folder)

          if self.install_database || Paser.boolean(ENV['MYSQL_RESET'])
            self.setup_database
          end

          protocol, user, password = self.build_profile.values_at(:protocol, :user, :password)
          pid = System.execute('mysqld_safe', { user: USER }, background: true)

          if false == self.check_database(protocol, user, password)
            self.shutdown_database(protocol, user, password)
            raise('database initial process failed.')
          end

          self.setup_replication
          Process.wait(pid)
        end
      end
    end
  end
end
